import React, { Component } from "react";
import Axios from "axios";
import Home from "./components/Home";
import Menu from "./components/Menu";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import AddData from "./components/AddData";
import ViewData from "./components/ViewData"
import 'bootstrap/dist/css/bootstrap.min.css';
export default class App extends Component {
  constructor() {
    super();
    this.state = { loading: true, data: [], isUpdate: false, };
  }
  componentDidMount() {
    Axios.get("http://110.74.194.124:15011/v1/api/articles")
      .then((res) =>
        this.setState({
          loading: false,
          data: res.data.DATA,
        })
      )
      .catch((err) => {
        alert(err)
      });
    console.log(this.state.data);
  }
  onUpdate = () => {
    this.setState({
      isUpdate: true,
    })
  }
  onDelete = (deleteData) => {
    let datas = this.state.data.filter((data) => data.ID !== deleteData);
    this.setState({
      data: datas,
    })
  }
  onAdd = () => {
    Axios.get("http://110.74.194.124:15011/v1/api/articles")
      .then((res) =>
        this.setState({
          loading: false,
          data: res.data.DATA,
        })
      )
      .catch((err) => {
        alert(err);
      });
    console.log(this.state.data)
  }
  render() {
    return (
      <div>
        <Router>
          <Menu />
          <Switch>
            <Route path="/add" render={() => <AddData onAdd={this.onAdd} />} />
            <Route path={"/view/:id"} render={(props) => <ViewData {...props} data={this.state.data} />} />
            <Home data={this.state.data} loading={this.state.loading} onDelete={this.onDelete} onAdd={this.onAdd} />
          </Switch>
        </Router>
      </div>
    );
  }
}