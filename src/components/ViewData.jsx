import React from "react";

export default function ViewData({ match, data }) {
  var VData = data.find((dt) => dt.ID == match.params.id);
  return (
    <div className="container">
      <h1>Article</h1>
      <div className="row"><div className="col-lg-4">
        <img src={VData.IMAGE} alt="" className="w-100 p-3" />
      </div>
        <div className="col-lg-8">
          <p>{VData.TITLE}</p>
          <p>{VData.DESCRIPTION}</p>
        </div>
      </div>
    </div>
  );
}