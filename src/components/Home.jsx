import React from "react";
import AllData from "./AllData";
import { Button } from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Home(props) {
  if (props.loading) {
    return <center><h3>Server Loading...</h3></center>;
    }
    return (
    <div  className="container">
      <center>
        <h2>Article Management</h2>
      </center>
      <center>
        <Button variant="dark" as={Link} to="/add">Add New Article</Button>
      </center> 
      <br />
      <AllData data={props.data} onDelete={props.onDelete}/>
    </div>
  );
}
