import React, { Component } from "react";
import { Table, Button } from "react-bootstrap";
import Axios from "axios";
import { Link} from "react-router-dom";


class AllData extends Component {
  constructor(props){
    super(props);
  }
  onDelete = (deleteData) => {
    if (window.confirm("Are you sure?")) {
      Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${deleteData}`)
        .then((res) => {
          alert(res.data.MESSAGE);
          console.log(res);
        })
        .catch((err) => {
          alert(err)
        });
        this.props.onDelete(deleteData);
    }
  };
  render() {
    const table = this.props.data.map((d) => (
      <tr key={d.ID}>
        <td>{d.ID}</td>
        <td>{d.TITLE}</td>
        <td>{d.DESCRIPTION}</td>
        <td>{d.CREATED_DATE}</td>
        <td>
          <div style={{ width: "90%", margin: "auto" }}>
            <Button as={Link} to={`/view/${d.ID}`} variant="primary" style={{ marginTop: "5px" }}>
              View
            </Button>{" "}
            <Button variant="warning" style={{ marginTop: "5px" }} as={Link} to={"/add"}>
              Edit
            </Button>{" "}
            <Button variant="danger" as={Link} to="/" style={{ marginTop: "5px" }}
            onClick={() => this.onDelete(d.ID)}>Delete</Button>
          </div>
        </td>
      </tr>
    ));
    return (
      <div>
        <Table striped bordered hover>
          <thead className="thead-dark">
            <tr className="text-center">
              <th>ID</th>
              <th style={{ width: "20%" }}>Title</th>
              <th style={{ width: "30%" }}>Description</th>
              <th>Create Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{table}</tbody>
        </Table>
      </div>
    );
  }
}
export default AllData;

