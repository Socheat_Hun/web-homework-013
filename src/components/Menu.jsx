import React from "react";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { Link } from "react-router-dom"


export default function Menu() {
  return (
    <div>
      <Navbar bg="dark" variant="dark">
      <div className="container">
      <Navbar.Brand as={Link} to="/">AMS</Navbar.Brand>
        <Nav className="mr-auto">
        <Nav.Link href="home">Home</Nav.Link>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-info">Search</Button>
        </Form>
        </div>
      </Navbar>
    </div>
  );
}




